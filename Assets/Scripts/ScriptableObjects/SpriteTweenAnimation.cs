﻿using DG.Tweening;
using UnityEngine;

namespace JeffSchoch.Dots
{
	/// <summary>
	/// This class is used to play a DoTween animation on a given spriterenderer.
	/// Base version does nothing, but is used as the generic reference type for more specific animations.
	/// Also contains some properties common to all animations.
	/// </summary>
	public class SpriteTweenAnimation : ScriptableObject
	{
		public float duration;
		public Ease ease;

		public virtual void SetDefaults(SpriteRenderer spriteRenderer) { }

		public virtual Tweener Animate(SpriteRenderer spriteRenderer)
		{
			return null;
		}
	}
}