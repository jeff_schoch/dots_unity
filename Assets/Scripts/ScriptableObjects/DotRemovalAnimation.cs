﻿using DG.Tweening;
using UnityEngine;

namespace JeffSchoch.Dots
{
	[CreateAssetMenu(fileName = "New Dot Removal Animation", menuName = "Dots/Animation/Removal")]
	public class DotRemovalAnimation : SpriteTweenAnimation
	{
		public float overshoot;
		public float finalScalar;

		public override void SetDefaults(SpriteRenderer spriteRenderer)
		{
			spriteRenderer.transform.localScale = Vector3.one;
		}

		public override Tweener Animate(SpriteRenderer spriteRenderer)
		{
			DOTween.Kill(spriteRenderer.transform);
			SetDefaults(spriteRenderer);
			return spriteRenderer.transform.DOScale(finalScalar, duration)
				.SetEase(ease, overshoot);
		}
	}
}