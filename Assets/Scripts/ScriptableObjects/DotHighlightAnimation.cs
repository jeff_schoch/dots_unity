﻿using DG.Tweening;
using UnityEngine;

namespace JeffSchoch.Dots
{
	[CreateAssetMenu(fileName = "New Dot Highlight Animation", menuName = "Dots/Animation/Highlight")]
	public class DotHighlightAnimation : SpriteTweenAnimation
	{
		public float finalScalar;
		public float startingAlpha;

		public override void SetDefaults(SpriteRenderer spriteRenderer)
		{
			// reset color to default
			Color color = spriteRenderer.color;
			color.a = startingAlpha;
			spriteRenderer.color = color;

			// reset scale to default
			spriteRenderer.transform.localScale = Vector3.one;
		}

		public override Tweener Animate(SpriteRenderer spriteRenderer)
		{
			DOTween.Kill(spriteRenderer);
			DOTween.Kill(spriteRenderer.transform);

			SetDefaults(spriteRenderer);

			Color finalColor = spriteRenderer.color;
			finalColor.a = 0;

			spriteRenderer.DOColor(finalColor, duration)
				.SetEase(ease);

			return spriteRenderer.transform.DOScale(finalScalar, duration)
				.SetEase(ease);
		}
	}
}