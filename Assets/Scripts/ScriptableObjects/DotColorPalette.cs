﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JeffSchoch.Dots {
	[CreateAssetMenu(fileName = "New Color Palette", menuName = "Dots/ColorPalette")]
	public class DotColorPalette : ScriptableObject
	{
		[Serializable]
		public class DotColor
		{
			[HideInInspector] public string label;
			public Dot.Type type;
			public Color32 color;
		}

		[SerializeField] private List<DotColor> colors;

		public Color GetColor(Dot.Type type)
		{
			return colors.Find(li => li.type == type).color;
		}

		private void OnValidate()
		{
			for(int i = 0; i < colors.Count; i++)
			{
				DotColor dc = colors[i];
				dc.label = dc.type.ToString();
			}
		}
	}
}