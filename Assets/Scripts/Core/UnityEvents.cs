﻿using System;
using UnityEngine.Events;

namespace JeffSchoch.Dots
{
	[Serializable] public class UnityEvent_DotCell : UnityEvent<DotCell> { }
}