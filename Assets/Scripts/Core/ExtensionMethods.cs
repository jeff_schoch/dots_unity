﻿using System.Collections.Generic;

public static class ExtensionMethods
{
	/// <summary>
	/// Allows queues to have multiple elements dequeued at the same time
	/// </summary>
	public static IEnumerable<T> DequeueChunk<T>(this Queue<T> queue, int chunkSize)
	{
		for (int i = 0; i < chunkSize && queue.Count > 0; i++)
		{
			yield return queue.Dequeue();
		}
	}
}
