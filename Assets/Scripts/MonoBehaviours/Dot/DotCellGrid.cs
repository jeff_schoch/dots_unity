﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace JeffSchoch.Dots
{
	/// <summary>
	/// This class is responsible for the creation of the game board, and general grid-related functions and retrievals.
	/// </summary>
	public class DotCellGrid : MonoBehaviour
	{
		[SerializeField] private Vector2Int gridDimensions;
		[SerializeField] private DotCellColumn columnPrefab;

		private DotPool dotPool;
		private Grid grid;
		private List<DotCellColumn> columns;

		[HideInInspector] public UnityEvent OnReady; // used to control initialization order
		public bool IsReady { get; private set; }

		public DotCell[][] Cells
		{
			get; private set;
		}

		public Grid Grid
		{
			get { return grid; }
			private set { grid = value; }
		}

		public DotPool DotPool
		{
			get { return dotPool; }
			private set { dotPool = value; }
		}

		private void Awake()
		{
			columns = new List<DotCellColumn>();
			Grid = GetComponent<Grid>();
			DotPool = GetComponentInChildren<DotPool>();
			Cells = new DotCell[gridDimensions.x][];
			CreateColumns();

			OnReady.Invoke();
			IsReady = true;
		}

		/// <summary>
		/// Clears and refills the entire board
		/// </summary>
		public void RefreshBoard()
		{
			foreach(DotCellColumn column in columns)
			{
				column.RefreshDots();
			}
		}

		/// <summary>
		/// Makes the whole board fill in empty cells with new dots.
		/// </summary>
		public void FillInEmptyCells()
		{
			foreach (DotCellColumn column in columns)
			{
				column.FillInEmptyCells();
			}
		}

		/// <summary>
		/// Returns the <see cref="DotCell"/> currently under the mouse.
		/// Returns null if there isn't one.
		/// </summary>
		public DotCell GetCellUnderMouse()
		{
			Vector2 mousePositionWorldSpace = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Vector3Int cellCoordinate = Grid.WorldToCell(mousePositionWorldSpace);
			return GetCell(cellCoordinate.x, cellCoordinate.y);
		}

		/// <summary>
		/// Returns true if the two given cells are adjacent in the grid.
		/// </summary>
		public bool CellsAreAdjacent(DotCell cellA, DotCell cellB)
		{
			return GetAdjacentCells(cellA).Contains(cellB);
		}

		/// <summary>
		/// Returns any adjacent <see cref="DotCell"/> objects around the given cell.
		/// </summary>
		public List<DotCell> GetAdjacentCells(DotCell cell) {
			Vector2Int coordinate = GetCellCoordinates(cell);
			return GetAdjacentCells(coordinate.x, coordinate.y);
		}

		/// <summary>
		/// Returns any adjacent <see cref="DotCell"/> objects around the given grid coordinates.
		/// </summary>
		public List<DotCell> GetAdjacentCells(int x, int y)
		{
			List<DotCell> adjacentCells = new List<DotCell>();

			// right
			DotCell adjacentCell = GetCell(x + 1, y);
			if(adjacentCell != null)
			{
				adjacentCells.Add(adjacentCell);
			}

			// up
			adjacentCell = GetCell(x, y + 1);
			if (adjacentCell != null)
			{
				adjacentCells.Add(adjacentCell);
			}

			// left
			adjacentCell = GetCell(x - 1, y);
			if (adjacentCell != null)
			{
				adjacentCells.Add(adjacentCell);
			}

			// down
			adjacentCell = GetCell(x, y - 1);
			if (adjacentCell != null)
			{
				adjacentCells.Add(adjacentCell);
			}

			return adjacentCells;
		}

		/// <summary>
		/// Returns the <see cref="DotCell"/> object at the given grid coordinates.
		/// Returns null if there isn't one.
		/// </summary>
		public DotCell GetCell(int x, int y)
		{
			if (IsValidGridCoordinate(x, y))
			{
				return Cells[x][y];
			}
			return null;
		}

		/// <summary>
		/// Returns the grid coordinates for the given cell
		/// </summary>
		public Vector2Int GetCellCoordinates(DotCell cell)
		{
			Vector3Int cellCoordinates = Grid.WorldToCell(cell.transform.position);
			return new Vector2Int(cellCoordinates.x, cellCoordinates.y);
		}

		/// <summary>
		/// Returns the <see cref="Dot"/> object at the given grid coordinates.
		/// Returns null if there isn't a <see cref="DotCell"/> at the coordinate.
		/// Returns null if there is no <see cref="Dot"/> inside the <see cref="DotCell"/> at the coordinate.
		/// </summary>
		public Dot GetDot(int x, int y)
		{
			// cell coordinates are in range
			if (IsValidGridCoordinate(x, y))
			{
				DotCell cell = columns[x].Cells[y];
				if (cell.HasDot)
				{
					return cell.Dot;
				}
			}
			return null;
		}

		/// <summary>
		/// Returns all <see cref="DotCell"/> objects containing a dot matching the given type.
		/// </summary>
		public List<DotCell> GetCellsContainingDotType(Dot.Type connectedDotType)
		{
			List<DotCell> matchingCells = new List<DotCell>();
			foreach(DotCellColumn column in columns)
			{
				matchingCells.AddRange(column.Cells.FindAll(cell => cell.HasDot && cell.Dot.DotType == connectedDotType));
			}
			return matchingCells;
		}

		private void CreateColumns()
		{
			CenterGridToOrigin();

			DotCellColumn column = null;
			for (int columnIndex = 0; columnIndex < gridDimensions.x; columnIndex++)
			{
				column = Instantiate(columnPrefab, transform);
				column.Initialize(this, columnIndex, gridDimensions.y);
				columns.Add(column);

				Cells[columnIndex] = column.Cells.ToArray();
			}
		}

		/// <summary>
		/// Returns true if the given grid coordinates are within the DotGrid bounds.
		/// </summary>
		private bool IsValidGridCoordinate(int x, int y)
		{
			return x >= 0 && x < gridDimensions.x && y >= 0 && y < gridDimensions.y;
		}

		/// <summary>
		/// center the grid on screen (does not account for grid gap)
		/// </summary>
		private void CenterGridToOrigin()
		{
			Vector3 centeredPosition = transform.position;
			centeredPosition.x -= gridDimensions.x * grid.cellSize.x * 0.5f;
			centeredPosition.y -= gridDimensions.y * grid.cellSize.x * 0.5f;
			transform.position = centeredPosition;
		}
	}
}