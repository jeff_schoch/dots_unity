﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace JeffSchoch.Dots
{
	/// <summary>
	/// This class receives input events from <see cref="DotCell"/> objects, and handles the connection logic & visuals
	/// </summary>
	public class DotCellConnector : MonoBehaviour
	{
		public class CellConnection
		{
			public DotCell start;
			public DotCell end;

			public CellConnection(DotCell start, DotCell end)
			{
				this.start = start;
				this.end = end;
			}
		}

		[SerializeField] private GameObject lineSegmentPrefab;
		[SerializeField] private float lineThickness;
		[SerializeField] private float delayBeforeRefill;

		private List<DotCell> cellsToClear;
		private List<CellConnection> cellConnections;
		private List<Transform> lineSegments;
		private Dot.Type connectedDotType;

		private CellConnection pendingConnection;
		private List<DotCell> adjacentToLastConnectedCell;
		private Transform currentSegment;

		private DotCellGrid dotGrid;

		public Color32 LineColor { get; private set; }

		public float LineThickness
		{
			get { return lineThickness; }
			set { lineThickness = value; }
		}

		private void Awake()
		{
			cellConnections = new List<CellConnection>();
			lineSegments = new List<Transform>();
			cellsToClear = new List<DotCell>();
			dotGrid = GetComponent<DotCellGrid>();
			adjacentToLastConnectedCell = new List<DotCell>();
		}

		private void OnEnable()
		{
			if (dotGrid.IsReady)
			{
				ListenForDragBegin();
			}
			else
			{
				dotGrid.OnReady.AddListener(ListenForDragBegin);
			}
		}

		private void OnDisable()
		{
			StopListeningForDrag();
		}

		private void ListenForDragBegin()
		{
			foreach (DotCell[] column in dotGrid.Cells)
			{
				foreach (DotCell cell in column)
				{
					cell.OnDragBegin.AddListener(OnCellDragBegin);
				}
			}
		}

		private void StopListeningForDrag()
		{
			foreach (DotCell[] column in dotGrid.Cells)
			{
				foreach (DotCell cell in column)
				{
					cell.OnDragBegin.RemoveListener(OnCellDragBegin);
					cell.OnDragEnter.RemoveListener(OnDragEnterAdjacentCell);
				}
			}
		}

		private void Update()
		{
			if (pendingConnection != null && pendingConnection.start != null)
			{
				if (Input.GetMouseButtonUp(0))
				{
					OnDragEnd();
				}
				else
				{
					Vector2 mousePositionWorldSpace = Camera.main.ScreenToWorldPoint(Input.mousePosition);
					UpdateLineSegment(currentSegment.transform, pendingConnection.start.transform.position, mousePositionWorldSpace);
				}
			}
		}

		private void OnCellDragBegin(DotCell cell)
		{
			// if the dragged cell owns a dot, and the dot is stationary in the cell
			if (cell.HasDot && !cell.Dot.IsMoving)
			{
				connectedDotType = cell.Dot.DotType;
				LineColor = cell.Dot.Color;

				ListenForAdjacentDragEnter(cell, connectedDotType);

				currentSegment = AddLineSegment(lineSegmentPrefab, LineColor);
				pendingConnection = new CellConnection(cell, null);
				cell.Dot.Highlight();
			}
		}

		private void ClearAdjacentReferences()
		{
			foreach (DotCell adjacentCell in adjacentToLastConnectedCell)
			{
				adjacentCell.OnDragEnter.RemoveListener(OnDragEnterAdjacentCell);
				adjacentCell.GetComponent<SpriteRenderer>().color = Color.white;
			}
			adjacentToLastConnectedCell.Clear();
		}

		private void ListenForAdjacentDragEnter(DotCell cell, Dot.Type type)
		{
			ClearAdjacentReferences();
			adjacentToLastConnectedCell = dotGrid.GetAdjacentCells(cell);
			foreach (DotCell adjacentCell in adjacentToLastConnectedCell)
			{
				if (adjacentCell.HasDot && adjacentCell.Dot.DotType == type)
				{
					adjacentCell.OnDragEnter.AddListener(OnDragEnterAdjacentCell);
				}
			}
		}

		private void UndoConnection()
		{
			if (pendingConnection != null && cellConnections.Count > 0)
			{
				CellConnection previousConnection = cellConnections.Last();
				cellConnections.Remove(previousConnection);

				pendingConnection = previousConnection;
				pendingConnection.end = null;

				lineSegments.Remove(currentSegment);
				Destroy(currentSegment.gameObject);

				currentSegment = lineSegments.Last();

				ListenForAdjacentDragEnter(pendingConnection.start, connectedDotType);
			}
		}

		// when a drag enters a cell adjacent to the current pending connected cell
		private void OnDragEnterAdjacentCell(DotCell cell)
		{
			// if the new cell is the previous in the list
			if (cellConnections.Count > 0 && cell == cellConnections.Last().start)
			{
				UndoConnection();
			}
			else if(!CellsAreConnected(pendingConnection.start, cell)) // if the cells are not already connected
			{
				ListenForAdjacentDragEnter(cell, connectedDotType);

				pendingConnection.end = cell;

				// add pending connection to completed connections
				cellConnections.Add(pendingConnection);

				Vector2 start = pendingConnection.start.transform.position;
				Vector2 end = pendingConnection.end.transform.position;

				UpdateLineSegment(currentSegment.transform, start, end);

				currentSegment = AddLineSegment(lineSegmentPrefab, LineColor);


				pendingConnection = new CellConnection(cell, null);

				// check if there are duplicate references, meaning a loop was formed
				if (CellConnectionCreatesLoop(cell))
				{
					// create a new list of all dots of this type in the grid
					cellsToClear = dotGrid.GetCellsContainingDotType(connectedDotType);
					foreach(DotCell connectedCell in cellsToClear)
					{
						connectedCell.Dot.Highlight();
					}
				}
				else
				{
					cell.Dot.Highlight();
				}

			}
		}

		/// <summary>
		/// Returns true if there is already an established connection between the two given cells.
		/// </summary>
		private bool CellsAreConnected(DotCell start, DotCell end)
		{
			return cellConnections.Any(connection =>
			(connection.start == start && connection.end == end)
			|| (connection.start == end && connection.end == start));
		}

		/// <summary>
		/// Returns true if the new cell is the starting point for another connection
		/// indicating a loop in the connections
		/// </summary>
		private bool CellConnectionCreatesLoop(DotCell newCell)
		{
			foreach(CellConnection connection in cellConnections)
			{
				if(connection.start == newCell)
				{
					return true;
				}
			}
			return false;
		}

		private void OnDragEnd()
		{
			ClearAdjacentReferences();

			for (int i = 0; i < lineSegments.Count; i++)
			{
				Destroy(lineSegments[i].gameObject);
			}
			lineSegments.Clear();

			currentSegment = null;
			pendingConnection = null;

			if (cellConnections.Count > 0)
			{
				StartCoroutine(RemoveConnectedDotsCoroutine());
			}

			cellsToClear.Clear();
			cellConnections.Clear();
		}

		private IEnumerator RemoveConnectedDotsCoroutine()
		{
			// aggregate connected cells into list
			foreach (CellConnection connection in cellConnections)
			{
				if (connection.start != null && connection.end != null)
				{
					if (!cellsToClear.Contains(connection.start))
					{
						cellsToClear.Add(connection.start);
					}
					if (!cellsToClear.Contains(connection.end))
					{
						cellsToClear.Add(connection.end);
					}
				}
			}

			// trigger removal of all appropriate cells
			foreach (DotCell connectedCell in cellsToClear)
			{
				connectedCell.Dot.RemoveWithAnimation();
			}

			yield return new WaitForSeconds(delayBeforeRefill);
			dotGrid.FillInEmptyCells();
		}

		private Transform AddLineSegment(GameObject prefab, Color32 color)
		{
			GameObject lineSpriteObject = Instantiate(prefab, parent: transform, instantiateInWorldSpace: true) as GameObject;
			SpriteRenderer lineSpriteRenderer = lineSpriteObject.GetComponent<SpriteRenderer>();
			if (lineSpriteRenderer != null)
			{
				lineSpriteRenderer.color = color;
				lineSegments.Add(lineSpriteObject.transform);
				return lineSpriteObject.transform;
			}
			return null;
		}

		private void UpdateLineSegment(Transform segment, Vector2 startPoint, Vector2 endPoint)
		{
			Vector2 midpoint = (startPoint + endPoint) * 0.5f;
			Vector2 diff = endPoint - startPoint;

			// set to midpoint
			segment.position = midpoint;

			// set alignment
			segment.right = diff;

			// scale to fit between points
			Vector3 newScale = segment.localScale;
			newScale.x = diff.magnitude;
			newScale.y = LineThickness;
			segment.localScale = newScale;
		}
	}
}