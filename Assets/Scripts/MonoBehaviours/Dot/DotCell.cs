﻿using UnityEngine;

namespace JeffSchoch.Dots
{
	/// <summary>
	/// This class represents a single cell in the grid.
	/// It fires events for drag input, and holds a reference to a dot on the grid.
	/// </summary>
	public class DotCell : MonoBehaviour
	{
		private static bool MOUSE_DOWN;

		#region Events
		[HideInInspector] public UnityEvent_DotCell OnDragBegin;
		[HideInInspector] public UnityEvent_DotCell OnDragEnter;
		#endregion Events

		private Dot dot;

		#region Properties
		public Dot Dot
		{
			get { return dot; }
			set
			{
				if(dot != value)
				{
					UpdateDotReference(value);
				}
			}
		}

		public bool HasDot
		{
			get { return Dot != null; }
		}
		#endregion Properties

		private void OnDotRemoved()
		{
			Dot = null;
		}

		private void UpdateDotReference(Dot newDot)
		{
			// clear current
			if (HasDot)
			{
				dot.OnRemoved.RemoveListener(OnDotRemoved);
			}

			// assign new
			dot = newDot;
			if (dot != null)
			{
				dot.OnRemoved.AddListener(OnDotRemoved);
			}
		}

		private void OnMouseDown()
		{
			OnDragBegin.Invoke(this);
			MOUSE_DOWN = true;
		}

		private void OnMouseUp()
		{
			MOUSE_DOWN = false;
		}

		private void OnMouseEnter()
		{
			if (MOUSE_DOWN)
			{
				OnDragEnter.Invoke(this);
			}
		}
	}
}