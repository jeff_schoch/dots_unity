﻿using System;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Collections;
using System.Linq;

namespace JeffSchoch.Dots
{
	/// <summary>
	/// This class represents a vertical column of dot cells
	/// </summary>
	public class DotCellColumn : MonoBehaviour
	{
		[SerializeField] private DotCell cellPrefab;
		[SerializeField] private float animationDuration;
		[SerializeField] private float perDotDelay;
		[SerializeField] private AnimationCurve animationEase;

		private DotCellGrid dotGrid;

		public Vector3 SpawnPoint { get; private set; }
		public List<DotCell> Cells { get; private set; }

		public void Initialize(DotCellGrid dotGrid, int index, int height)
		{
			this.dotGrid = dotGrid;
			Cells = new List<DotCell>();
			SetHeight(height);

			Vector3 worldPosition = Vector3.zero;
			Vector3Int gridPosition = Vector3Int.zero;
			gridPosition.x = index;
			transform.position = dotGrid.Grid.GetCellCenterWorld(gridPosition);
			SpawnPoint = GetSpawnPoint();

			DotCell newCell = null;
			for (int i = 0; i < height; i++)
			{
				gridPosition.y = i;
				worldPosition = dotGrid.Grid.GetCellCenterWorld(gridPosition);

				newCell = Instantiate(cellPrefab, parent: transform, worldPositionStays: true);
				newCell.transform.position = worldPosition;

				Cells.Add(newCell);
			}

			RefreshDots();
		}

		public void RefreshDots()
		{
			for (int i = 0; i < Cells.Count; i++)
			{
				DotCell cell = Cells[i];
				Dot dot = null;
				if (cell.HasDot)
				{
					dot = cell.Dot;
				}
				else
				{
					dot = dotGrid.DotPool.GetObject();
				}

				ResetDot(dot);
				AnimateDotToCell(dot, cell, i * perDotDelay);
				cell.Dot = dot;
			}
		}

		public void FillInEmptyCells()
		{
			for (int i = 0; i < Cells.Count; i++)
			{
				DotCell emptyCell = Cells[i];
				if (!emptyCell.HasDot)
				{
					// find the next occupied cell
					DotCell nextOccupiedCell = null;
					for (int j = i + 1; j < Cells.Count; j++)
					{
						DotCell nextCell = Cells[j];
						if (nextCell.HasDot && !nextCell.Dot.IsMoving)
						{
							nextOccupiedCell = nextCell;
							break;
						}
					}

					if (nextOccupiedCell != null)
					{
						// shift animate it down, update references
						AnimateDotToCell(nextOccupiedCell.Dot, emptyCell, i * perDotDelay);
						emptyCell.Dot = nextOccupiedCell.Dot;
						nextOccupiedCell.Dot = null;
					}
					else
					{
						// there are no more cells with dots, get more from pool
						Dot dot = dotGrid.DotPool.GetObject();
						ResetDot(dot);
						AnimateDotToCell(dot, emptyCell, i * perDotDelay);
						emptyCell.Dot = dot;
					}
				}
			}
		}

		private void AnimateDotToCell(Dot dot, DotCell cell, float delay = 0)
		{
			dot.transform.DOMoveY(cell.transform.position.y, animationDuration)
				.SetEase(animationEase)
				.SetDelay(delay);
		}

		private void ResetDot(Dot dot)
		{
			dot.transform.DOKill();
			dot.transform.position = SpawnPoint;
			dot.ResetAnimations();
			dot.SetRandomType();
		}

		private void SetHeight(int height)
		{
			Vector3 newScale = transform.localScale;
			newScale.y = height;
			transform.localScale = newScale;
		}

		private Vector3 GetSpawnPoint()
		{
			Vector2 spawnPoint = transform.position;
			Vector2 topRightWorld = Camera.main.ViewportToWorldPoint(Vector2.one);
			spawnPoint.y = topRightWorld.y + 1f;
			return spawnPoint;
		}
	}
}