﻿namespace JeffSchoch.Dots
{
	/// <summary>
	/// Implemented object pool which spawns Dot prefabs.
	/// This class allows dots to be automatically added back into the pool when removed.
	/// </summary>
	public class DotPool : ObjectPool<Dot>
	{
		public override Dot GetObject(bool createIfEmpty = true)
		{
			Dot dot = base.GetObject(createIfEmpty);
			dot.OnRemoved.RemoveAllListeners();
			// automatically add back to pool when removed
			dot.OnRemoved.AddListener(() => AddObject(dot));
			return dot;
		}
	}
}