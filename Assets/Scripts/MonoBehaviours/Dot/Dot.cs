﻿using DG.Tweening;
using System;
using UnityEngine;
using UnityEngine.Events;

namespace JeffSchoch.Dots
{
	/// <summary>
	/// This class represents a single dot in the board.
	/// Each dot has a type which is useful for matching colors, and each dot can randomize its type on demand.
	/// </summary>
	public class Dot : MonoBehaviour
	{
		private static int NUM_TYPES = 0; // auto assigned based on enum length

		public enum Type
		{
			None = 0,
			A    = 1,
			B    = 2,
			C    = 3,
			D    = 4,
			E    = 5,
		}

		[SerializeField] private DotColorPalette colorPalette;

		[SerializeField] private SpriteRenderer highlightObject;
		[SerializeField] private SpriteTweenAnimation highlightAnimation;
		[SerializeField] private SpriteTweenAnimation removalAnimation;

		private SpriteRenderer spriteRendererComponent;
		private Type dotType;

		[HideInInspector] public UnityEvent OnRemoved;

		public bool IsMoving
		{
			get { return DOTween.IsTweening(transform); }
		}

		public Color32 Color
		{
			get { return spriteRendererComponent.color; }
		}

		public Type DotType
		{
			get { return dotType; }
			set
			{
				if (value != dotType)
				{
					dotType = value;
					ChangeType(value);
				}
			}
		}

		private void Awake()
		{
			NUM_TYPES = Enum.GetValues(typeof(Type)).Length;
			spriteRendererComponent = GetComponent<SpriteRenderer>();
		}

		private void Start()
		{
			SetRandomType();
		}

		public void SetRandomType()
		{
			 ChangeType();
		}

		public void Remove()
		{
			OnRemoved.Invoke();
		}

		public void ResetAnimations()
		{
			removalAnimation.SetDefaults(spriteRendererComponent);
			highlightAnimation.SetDefaults(highlightObject);
		}

		public Tweener RemoveWithAnimation()
		{
			OnRemoved.Invoke();
			return removalAnimation.Animate(spriteRendererComponent);
		}

		public void Highlight()
		{
			highlightAnimation.Animate(highlightObject);
		}

		private void ChangeType(Type type = Type.None)
		{
			if (type == Type.None)
			{
				type = (Type)UnityEngine.Random.Range(1, NUM_TYPES);
			}

			DotType = type;
			spriteRendererComponent.color = colorPalette.GetColor(type);
			highlightObject.color = spriteRendererComponent.color;
		}
	}
}