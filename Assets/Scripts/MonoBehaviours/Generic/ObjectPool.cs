﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace JeffSchoch
{
	/// <summary>
	/// Generic class used to maintain a pool of objects for runtime reuse.
	/// </summary>
	public class ObjectPool<T> : MonoBehaviour
	{
		[SerializeField] private GameObject prefab;
		[SerializeField] private int initialObjectCount;
		[SerializeField] private Transform holdingLocation;
		protected Queue<T> pool = new Queue<T>();

		private void Awake()
		{
			CreateObjects(initialObjectCount);
		}

		public virtual void AddObject(T go)
		{
			pool.Enqueue(go);
		}

		public virtual T GetObject(bool createIfEmpty = true)
		{
			if (pool.Count <= 0)
			{
				if (createIfEmpty)
				{
					CreateObjects(1);
				}
				else
				{
					return default(T);
				}
			}
			return pool.Dequeue();
		}

		public List<T> GetObjects(int count, bool createIfEmpty = false)
		{
			// if the pool size is too small, add more objects
			if (pool.Count <= count)
			{
				if (createIfEmpty)
				{
					SetPoolSize(count);
				}
				else
				{
					return new List<T>();
				}
			}

			return pool.DequeueChunk(count).ToList();
		}

		public void SetPoolSize(int count)
		{
			if(pool.Count < count)
			{
				CreateObjects(count - pool.Count);
			}
		}

		protected virtual void CreateObjects(int count)
		{
			T newObject;
			for (int i = 0; i < count; i++)
			{
				newObject = Instantiate(prefab, holdingLocation.position, Quaternion.identity, parent: transform).GetComponent<T>();
				pool.Enqueue(newObject);
			}
		}
	}
}